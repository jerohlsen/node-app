var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');

var bodyParser= require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

var cookies = require('node-cookie');

/// create model
var Post = require('../models/post');

/// create model
var Artist = require('../models/artist');


var limiter = 6;

/// having no var makes it GLOBAL (& therefore accessible from users.js)
firstLoadHome = 0;

// Get Homepage
router.get('/home', ensureAuthenticated, function(req, res){

	var username = req.user.username;

	if(firstLoadHome==0){
		cookies.create(res, "show", "all", '16charlongsecret');

		firstLoadHome=1;
	}
	
	res.render('index', {username: username});
		
});




router.get('/',  function(req, res){

	res.redirect('/videos');

});


router.get('/videos',  function(req, res){

	var username;

	if(req.user){
		username = req.user.username;
	}else{
		username = "cualquiera";
	}
	
	//console.log(req.query.event);
	if(req.query.event=="scrollBottom"){
		skipping+=limiter;
	}else{
		skipping=0;
	}

	if(cookies.get(req,"show")=="user" && req.user){
		Post.find({postUSER: req.user.username, postTYPE: "video"}, function(err, data){				

			if(err){ throw err;

					}else{

				if(req.query.event=="scrollBottom"){
					res.render('videospaginated', {layout: false, username: username, posts:data});
				}else{
					res.render('videos', {username: username, posts:data});
				}
			}					
		}).sort({_id:-1}).skip(skipping).limit(limiter);
	}else{
		Post.find({postTYPE: "video"}, function(err, data){				

			if(err){ throw err;

			}else{
				if(req.query.event=="scrollBottom"){
					res.render('videospaginated', {layout: false, username: username, posts:data});
				}else{
					res.render('videos', {username: username, posts:data});
				}
			}					
		}).sort({_id:-1}).skip(skipping).limit(limiter);
	}


});



router.get('/albums',  function(req, res){

	var username;

	if(req.user){
		username = req.user.username;
	}else{
		username = "cualquiera";
	}

	if(req.query.event=="scrollBottom"){
		skipping+=limiter;
	}else{
		skipping=0;  /// if back button???
	}
	
		if(cookies.get(req,"show")=="user" && req.user){
			Post.find({postUSER: req.user.username, postTYPE: "video", postSUBTYPE: "album"}, function(err, data){				

				if(err){ throw err;

				}else{
					if(req.query.event=="scrollBottom"){
						res.render('albumspaginated', {layout: false, username: username, posts:data});
					}else{
						res.render('albums', {username: username, posts:data});
					}
				}					
			}).sort({_id:-1}).skip(skipping).limit(limiter);
		}else{
			Post.find({postTYPE: "video", postSUBTYPE: "album"}, function(err, data){				

				if(err){ throw err;

				}else{
					if(req.query.event=="scrollBottom"){
						res.render('albumspaginated', {layout: false, username: username, posts:data});
					}else{
						res.render('albums', {username: username, posts:data});
					}
				}					
			}).sort({_id:-1}).skip(skipping).limit(limiter);
		}
	/// get POSTS  where user = this.user

});





router.get('/varios',  function(req, res){


	var username;

	if(req.user){
		username = req.user.username;
	}else{
		username = "cualquiera";
	}

	if(req.query.event=="scrollBottom"){
		skipping+=limiter;
	}else{
		skipping=0;
	}
	
		if(cookies.get(req,"show")=="user" && req.user){
			Post.find({postUSER: req.user.username, postTYPE: "video", postSUBTYPE: "no-album"}, function(err, data){				

				if(err){ throw err;

				}else{
					if(req.query.event=="scrollBottom"){
						res.render('no-albumspaginated', {layout: false, username: username, posts:data});
					}else{
						res.render('no-albums', {username: username, posts:data});
					}
				}					
			}).sort({_id:-1}).skip(skipping).limit(limiter);
		}else{
			Post.find({postTYPE: "video", postSUBTYPE: "no-album"}, function(err, data){				

				if(err){ throw err;

				}else{
					if(req.query.event=="scrollBottom"){
						res.render('no-albumspaginated', {layout: false, username: username, posts:data});
					}else{
						res.render('no-albums', {username: username, posts:data});
					}
				}					
			}).sort({_id:-1}).skip(skipping).limit(limiter);
		}


	/// get POSTS  where user = this.user

});



router.get('/videos/:name', function(req, res){

	skipping=0;

	if(req.user){
		username = req.user.username;
	}else{
		username = "cualquiera";
	}

	Post.find({postID: req.params.name}, function(err, data){

		if(err){ 

			throw err;

		}else{

			
			res.render('posts-solo', {posts:data, username: username});
		}
	});

});


/// appending to PAGE (instead of jumping to diff one)
router.post('/vids', urlencodedParser, function(req, res){

	skipping=0;

	req.body = {data: req.body.data}

	console.log(req.body.data);

	Post.find({postNAME:req.body.data}, function(err, data){

		if(err){ 

			throw err;

		}else{

			for (var i =0; i < data.length; i++) {
				console.log("CUANTO ES I ? "+i);
				console.log(data[i]);
			}
			console.log("NAME>>>>>> "+data[0].postTYPE);

			var datum = {
				postID: data[0].postID,
				postUSER:data[0].postUSER, 
				postNAME:data[0].postNAME, 
				postARTIST: data[0].postARTIST,
				postURL:data[0].postURL,
				postTYPE:data[0].postTYPE, 
				postSUBTYPE:data[0].postSUBTYPE, 
				postDESCRIPTION:data[0].postDESCRIPTION
				}
			
			res.json(datum);

			//res.render('posts-solo', {posts:data});
		}
	});

});



router.get('/artists', function(req, res){

	skipping=0;


	Artist.find( function(err, data){

		if(err){ 

			throw err;

		}else{
			res.render('artists', {posts:data});
		}
	});

});




router.get('/artists/:name', function(req, res){

	skipping=0;


	// para resolver el problema con "?" en URL

////  tengo que agarrar req.params.name >> pasar por for loop letra por letra, 
///																		contar en que indexOf esta el "?"
///																				 poner de vuelta, una por una en otra variable
//																							y entonces agregar "\" justo antes del "?"
//																					o en otras palabras 'escape it', si es que se puede en la URL

///  or maybe https://stackoverflow.com/questions/3116796/replacing-20-with-a-dash
// turning '%20' into  '-'  ...etc   

//https://webmasters.stackexchange.com/questions/90615/is-it-valid-to-include-digits-and-question-marks-in-the-url-slug-for-a-permalink
/// ""So if you want to include a question mark in your slug, you have to percent-encode it with %3F

	Post.find({postARTIST: req.params.name}, function(err, data){

		if(err){ 

			throw err;

		}else{
			res.render('videos', {posts:data});
		}
	});

});



	router.post('/artist-autocomplete', function(req, res){

		var artists= [];
									
		Artist.find({}, function(err, data){

			if(err) throw err;


			for (var i=0; i<data.length; i++) {
				//console.log(data[i].artistNAME);

				artists.push(data[i].artistNAME);
			}

			res.json(artists);
		});
		
	
		
	});


router.post('/video', urlencodedParser, function(req, res){
		
	req.body={
		postID: Post(req.body.id),
		postUSER:req.user.username, 
		postNAME:req.body.postNAME, 
		postARTIST: req.body.postARTIST,
		postURL:req.body.postURL,
		postTYPE:req.body.postTYPE, 
		postSUBTYPE:req.body.postSUBTYPE, 
		postDESCRIPTION:req.body.postDESCRIPTION
	};


	var artist = {
		artistNAME : req.body.postARTIST, 
		artistLOCATION:null, 
		artistFORMED:null, 
		artistGENRES: null, 
		artistDESCRIPTION: null
	};

	var artistExists=false;

	var newPost = Post(req.body).save(function(err, data){
		if(err) throw err;

			
		Artist.find({artistNAME: req.body.postARTIST}, function(err, data){

		
			if(err) throw err;
					
				

				for (var i =0;i<data.length; i++) {

					if(artistExists !=true){
						if(data[i].artistNAME==req.body.postARTIST){
							artistExists=true;
						}
					}		
				}

			if(artistExists==false){
				var newArtist = Artist(artist).save(function(err, data){
					if(err) throw err;

					res.render('index', {username: req.body.name});
				});	
			}else{
				res.render('index', {username: req.body.name});
			}

		});

	});		

	

});



router.post('/user-filter', urlencodedParser, function(req, res){
		
	if(cookies.get(req,"show")=="user"){

		
		cookies.create(res, "show", "all", '16charlongsecret');

		res.json(null);

	}else{
		
		cookies.create(res, "show", "user", '16charlongsecret');

		res.json(null);

	}

	console.log("COOKY SET TO  "+cookies.get(req,"show"));
	
		
});


// router.post('/back', urlencodedParser, function(req, res){
		
// 	console.log("Y???? " +skipping);
	
// 	goneback = true;
		
// 	res.json(null);

	
// });




	
router.delete('/post/:itemID', function(req, res){
		
		console.log(req.params.itemID);

		//delete requested Item from mongo DB
		Post.find({postID: req.params.itemID}).remove(function(err, data){

			if(err) throw err;

			res.json(data);

		});

		// data = data.filter(function(todo){
		// 	return todo.item.replace(/ /g, "") !== req.params.item;
		// });

		// res.json(data);
	});


router.patch('/post/:itemID', urlencodedParser, function(req, res){
		
		

		req.body = {
			title:req.body.title,
			text: req.body.text
		}

		
		Post.findOneAndUpdate({postID: req.params.itemID}, {$set:{postNAME : req.body.title, postDESCRIPTION : req.body.text}},  function(err, data){

			if(err) throw err;


			res.json(data);

		});
		

	});



function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		//req.flash('error_msg','You are not logged in');
		res.redirect('/users/login');
	}
}

module.exports = router;