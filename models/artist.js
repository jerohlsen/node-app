var mongoose = require('mongoose');

/// create data blueprint (Schema)
var artistSchema = new mongoose.Schema({
	artistNAME:String, 
	artistLOCATION:String, 
	artistFORMED:String, 
	artistGENRES: String, 
	artistDESCRIPTION: String
});


/// create model
var Artist  = module.exports = mongoose.model('Artist', artistSchema);

