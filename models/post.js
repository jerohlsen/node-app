var mongoose = require('mongoose');

/// create data blueprint (Schema)
var postSchema = new mongoose.Schema({
	postID:String, 
	postUSER:String, 
	postNAME:String, 
	postARTIST:String, 
	postURL:String,
	postTYPE: String, 
	postSUBTYPE: String, 
	postDESCRIPTION: String
});

var postID = postSchema._id;

/// create model
var Post  = module.exports = mongoose.model('Post', postSchema);

